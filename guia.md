# HealthDashboard

Este documento serve como uma introdução rápida da plataforma HealthDashboard, guiando o usuário por suas funcionalidades principais.

## Inicial

Ao abrir o site da versão de homologação da plataforma, entre na página de "Mapa e Filtros" clicando no botão presente na parte superior da página. Ao acessar a página será necessário login, basta adicionar as credenciais fornecidas no e-mail.

![](./prints/principal/0_limpo.png)
![](./prints/principal/1_login.png)

\newpage
## Mapa e Filtros

Página principal de visualização da HealthDashboard. Nela estão presentes as opções de filtros dos dados e o mapa principal, assim como suas opções de visualização.

### Visão Inicial

Ao entrar na página temos uma visão semelhante à esta:

![](./prints/mapa/0_limpo.png)

À esquerda temos o mapa, contendo controle de zoom (canto superior esquerdo), minimapa interativo (canto inferior direito) e dois menus colapsáveis (canto inferior esquerdo), um para as legendas do mapa e outro para as opções de visualização selecionadas. À direita temos, em ordem, sessões para os filtros, as opções do mapa e opções de exportação dos dados.

![](./prints/mapa/1_legenda.png)

Em qualquer momento podemos clicar nos estabelecimentos para abrir um tooltip com informações sobre ele, incluindo nome, número de leitos e dados administrativos.

![](./prints/mapa/0_5_estabelecimento_tooltip.png)

Os dados carregados na versão atual de homologação da plataforma são da cidade de São Paulo, porém a HealthDashboard	funciona com dados SIH-SUS de qualquer região do país, desde que sejam formatados adequadamente.

Para facilitar referência durante o uso, podemos habilitar a visualização dos limites da cidade dentro do acordeão "limites administrativos", como demonstra a figura.

![](./prints/mapa/2_limites_cidade.png)


### Realizando Buscas

A principal funcionalidade da plataforma são os filtros compostos que podemos adicionar aos dados. Os filtros abrangem uma vasta quantidade de campos do dataset e podem ser compostos livremente, garantindo assim análises com alto grau de aprofundamento.

Eles ficam agrupados em três acordeões, um para campos relacionados aos estabelecimentos, como tipo de gestão ou número de leitos, um para relacionados aos procedimentos, como diagnóstico ou período de ocorrência, e um último para informações dos pacientes, como raça/cor ou distância deslocada até o estabelecimento.

As seguintes imagens demonstram como se pode fazer uma busca na plataforma.  Nela serão selecionados procedimentos com diagnóstico principal envolvendo infartos no miocárdio e complicações (CIDs: I21, I22, I23), entre janeiro de 2018 até dezembro de 2019, onde o paciente teve que percorrer mais de 20km para chegar ao estabelecimento.

No acordeão "Procedimentos":

\begin{center} 
\includegraphics[height=0.34\textheight]{./prints/mapa/3_diagnostico.png}
\end{center}

\begin{center} 
\includegraphics[height=0.34\textheight]{./prints/mapa/4_periodo.png}
\end{center}

No acordeão "Informações do Paciente":

![](./prints/mapa/5_distancia.png)

Após selecionar os filtros desejados, clicamos no botão "Buscar" para realizar a busca.

Nota: O último acordeão define os parâmetros usados no cálculo das taxas do mapa de calor por taxa de internações, seu funcionamento e as suas opções serão explicados mais adiante.

### Opções de visualizações

Com os procedimentos carregados, por padrão o mapa mostra apenas os estabelecimentos e os procedimentos agrupados em clusters usando raio de 5km, porém essa visualização pode ser customizada.

No primeiro acordeão da sessão "Opções do Mapa" podemos desabilitar a visão dos procedimentos por agrupamentos e alterar o raio usado para agrupá-los.

![](./prints/mapa/6_agrupamento.png)

Ao dar zoom suficiente ou ao clicá-los os clusters são separados em grupos menores, isso pode ser feito até termos os ícones dos procedimentos individuais. Neste ponto é possível clicar para abrir um tooltip com mais informações, semelhante aos estabelecimentos.

\begin{center} 
\includegraphics[height=0.37\textheight]{./prints/mapa/6_5_agrupamento_tooltip.png}
\end{center}

No segundo acordeão encontramos as opções relacionadas aos mapas de calor, na HealthDashboard temos dois tipos de mapa de calor. O primeiro por número absoluto de internações, calculado a partir dos próprios procedimentos com pontos gerados nas suas respectivas coordenadas, e o segundo por taxa de internações, calculado a partir de taxas calculadas durante as buscas e agrupadas por setor censitário ou distrito administrativo.

Ambos os mapas de calor compartilham das mesmas opções, podemos habilitá-los, alterar o raio usado para calculá-los, habilitar o modo de alto contraste e alterar a opacidade.

Mapa de calor por número absoluto de internações:

\begin{center} 
\includegraphics[height=0.37\textheight]{./prints/mapa/7_heatmap_absoluto.png}
\end{center}

Mapa de calor por taxa de internações:

![](./prints/mapa/8_heatmap_ponderado.png)

A taxa de internações no segundo mapa de calor é definida pela razão entre a quantidade de procedimentos no setor em comparação com a população filtrada do mesmo setor. O agrupamento de setores pode ser feito por setor censitário (padrão) ou por distrito administrativo, e a população considerada pode ser filtrada por sexo, raça/cor e faixa etária. Caso nada seja selecionado, a população inteira do setor é considerada (caso da pesquisa anterior).

Os parâmetros para calcular a taxa devem ser definidos durante a busca, assim como os filtros, e podem ser encontrados no acordeão "Mapa de calor por taxa de internações". As suas opções são as seguintes:

\begin{center} 
\includegraphics[height=0.425\textheight]{./prints/mapa/8_5_heatmap_ponderado_opcoes.png}
\end{center}

O próximo acordeão já foi explorado levemente, o que pode ser adicionado é que a HealthDashboard é capaz de mostrar outros limites além da cidade e que ao clicar em um limite específico um tooltip com o nome e o total de procedimentos ocorridos nele é mostrado.

![](./prints/mapa/9_limites_crs_tooltip.png)

Por fim, também podemos habilitar a visão dos raios de abrangência dos procedimentos. Com isso o mapa mostra três circunferências, uma azul, verde e vermelha, com raios iguais aos percentis de 25%, 50% e 75% das distancias de deslocamento dos pacientes até o respectivo estabelecimento, respectivamente.

As seguintes imagens descrevem um caso útil para essa visualização. Podemos limpar os filtros com o botão e buscar apenas por procedimentos de dois estabelecimentos, o Hospital Das Clínicas, que atende pessoas da cidade inteira, e o Hospital Estadual de Itaim Paulista, mais localizado para a zona leste de São Paulo.

![](./prints/mapa/10_estabelecimentos_busca.png)
![](./prints/mapa/11_estabelecimentos_percentis.png)

### Exportação

Tanto os dados dos procedimentos filtrados quanto a visualização do mapa em si podem ser exportados para download na última sessão da página.

Exportando por CSV uma tabela é gerada com os procedimentos filtrados, composta por todos os campos dos dados de entrada SIH. Esses dados assim podem ser disponibilizados em algum relatório ou analisados em algum programa externo.

Exportando por PDF um documento com a visualização do mapa é gerada, assim os resultados visuais das análises podem ser facilmente incorporados em relatórios.

Nota: Houve mudanças consideráveis na visualização do mapa nas últimas atualizações da plataforma e, por isso, o PDF exportado pode apresentar alguns erros, estes serão concertados nas próximas versões.

### Exploração

Isso conclui o tour pelas funcionalidades da página principal da HealthDashboard. Com isso, caso tenha interesse, fazemos o covite para interagir livremente com o mapa, realizando algumas buscas mais complexas que despertem curiosidade afim de explorar mais as funcionalidades mapeadas.

Quando estiver satisfeito(a), podemos seguir para a próxima, e última, página de visualização da plataforma, a página de "Dados e Gráficos".

Nota: Nos últimos meses foi realizado um grande esforço para melhorar a performance das buscas, porém o mesmo ainda não foi feito com a próxima página. Dessa forma recomendamos que faça uma busca com poucos resultados antes de acessá-la para perder pouco tempo durante o carregamento. Isso é feito ainda na página do mapa porque que ambas compartilham os mesmos filtros.

\newpage
## Dados e Gráficos

Esta página auxiliar da HealthDashboard tem como objetivo fornecer visões mais aprofundada de certas características dos dados filtrados obtidos na página de mapas.

Ela é composta por sessões com diferentes visualizações dos dados, agrupadas de acordo com o tipo de informação presente nela, como procedimentos por especialidades ou distâncias de deslocamento dos pacientes, tanto geral quanto por especialidades.

Dessa forma, a página tem foco em complementar a visualização principal do mapa com informações que podem ser úteis em durante as análises.

Os acordeões dessa página podem ser exportados como PDF, assim como o mapa, dessa forma eles também podem servir para enriquecer relatórios e documentos.

Segue imagens com a visão inicial da página e alguns exemplos de sessões dela usando a última busca realizada neste guia:

\begin{center} 
\includegraphics[height=0.35\textheight]{./prints/graficos/0_filtros.png}
\end{center}

\begin{center} 
\includegraphics[height=0.35\textheight]{./prints/graficos/1_dist_especialidade.png}
\end{center}

![](./prints/graficos/3_distancias.png)
![](./prints/graficos/2_diagnostico_principal.png)
