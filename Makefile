make:
	pandoc \
		-V fontsize=12pt -V papersize:a4 -V geometry:margin=.5in \
		guia.md -o out.pdf
	evince out.pdf > /dev/null 2>&1 &
